public class ContactAndLeadSearch{

     public static List<List<SObject>> searchContactsAndLeads(String Name){
    
    List<List<SObject>> result= [FIND :name IN ALL FIELDS RETURNING Contact(LastName), Lead(LastName)]; return result; }
}