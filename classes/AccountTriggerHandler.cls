public with sharing class AccountTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
     // Grab your Custom Setting values 
     
    Account_Case_Method__c ACM = Account_Case_Method__c.getInstance('OnOff');
    String Switched1 = ACM.Switched_1__c;
    String Switched2 = ACM.Swtiched_2__c;
    //System.debug(System.LoggingLevel.ERROR, Switched1);
    //system.debug(Switched2);    


    
    public AccountTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
        
    public void OnBeforeInsert(Account[] newAccounts){
        //Example usage
        for(Account newAccount : newAccounts){
            if(newAccount.AnnualRevenue == null){
                newAccount.AnnualRevenue.addError('Missing annual revenue');
            }
        }
    }
    
    public void OnAfterInsert(Account[] newAccounts){
      //Create New Opp      
      for(Account newAccount : newAccounts){
        Opportunity newOne = new Opportunity();
        newOne.Name = 'Sample 123';
        newOne.CloseDate = Date.Today();
        newOne.StageName = 'Prospecting';
        newOne.TrackingNumber__c = '1';
        newOne.AccountId = newAccount.Id;
        insert newOne;
      }
      
      //create new Case
      for(Account newAccount : newAccounts){
        Case shirtCase = new Case(); 
        shirtCase.Subject = 'Send them a free T-shirt!'; 
        shirtCase.Priority = 'Ultra High';
        shirtCase.AccountId = newAccount.Id;
        insert shirtCase;
      }
      
      
    }
    
    @future public static void OnAfterInsertAsync(Set<ID> newAccountIDs){
        //Example usage
        List<Account> newAccounts = [select Id, Name from Account where Id IN :newAccountIDs];
    }
    
    public void OnBeforeUpdate(Account[] oldAccounts, Account[] updatedAccounts, Map<ID, Account> accountMap){
        //Example Map usage
        Map<ID, Contact> contacts = new Map<ID, Contact>( [select Id, FirstName, LastName, Email from Contact where AccountId IN :accountMap.keySet()] );
        
           for(Account isdelacct : updatedAccounts){
             //  if(isdelacct.IsDeleted) {
                   isdelAcct.addError('Cannnot delete it before update');
              // } 
           }
           
    }
    
    public void OnAfterUpdate(Account[] oldAccounts, Account[] updatedAccounts, Map<ID, Account> accountMap){
        
    }
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedAccountIDs){
        List<Account> updatedAccounts = [select Id, Name from Account where Id IN :updatedAccountIDs];
    }
    
    public void OnBeforeDelete(Account[] accountsToDelete, Map<ID, Account> accountMap){ 
    
     /*   Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
            for(Account delacct : accountsToDelete){
                if(profileName != 'Test') {
                delAcct.addError('Cannnot delete it');
                //delAcct.AnnualRevenue.addError('Cannnot delete');
                //newAccount.addError('Missing annual revenue');
                }
            }      
     */                
    }
    
    public void OnAfterDelete(Account[] deletedAccounts, Map<ID, Account> accountMap){
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedAccountIDs){
        
    }
    
    public void OnUndelete(Account[] restoredAccounts){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }
}