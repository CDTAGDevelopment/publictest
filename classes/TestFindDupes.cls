@isTest
public class TestFindDupes {
    static testMethod void testDupes() {
    
    //Let's create our record from scratch!
    Contact c= new Contact();
    c.FirstName = 'Dave';
    c.LastName = 'Jones';
    c.Email = 'davejones@email.com';
    insert c;
    
    // now let's create a dupe lead
    Lead dupeLead = new Lead();
    dupeLead.FirstName = 'Dave';
    dupeLead.LastName = 'Jones';
    dupeLead.Company = 'IBM';
    dupeLead.Email = 'davejones@email.com';
    
    //This is a special wayu of doing "risky" things
    try {
        insert dupeLead;
    } catch (Exception e) {
        System.debug('An error happened, as predicted!');        
    }
    
    //now we try to find our duple lead by email
    List<Lead> dupes = [Select Id From Lead
                        Where Email = 'davejones@email.com'];
    System.assertEquals(0,dupes.size());                    
    
    //now we break out trigger by insterting a non dupe
    Lead legitLead = new Lead();
    legitLead.FirstName = 'Dave';
    legitLead.LastName = 'Smith';
    legitLead.Company = 'HP';
    legitLead.Email = 'davesmith@email.com';
    insert legitLead; 
    
    //now we try to find out leget lead by email
    List<Lead> legits = [SELECT Id FROM Lead 
                        WHERE Email = 'davesmith@email.com'];
       System.assertEquals(1, legits.size());
     
    }
}