@IsTest
public class TestOMgr {
  static testmethod void testOppMgrs() {
    // Create manager
    User mgr              = new User();
    mgr.Username          = 'govlimitking@gmail.com';
    mgr.Email             = 'govlimitking@gmail.com';
    mgr.FirstName         = 'No';
    mgr.LastName          = 'Worries';
    mgr.Alias             = 'ez';
    mgr.CommunityNickname = 'lifeisgood';
    mgr.ProfileId         = '00ei0000000rTfp';
    mgr.TimeZoneSidKey    = 'GMT';
    mgr.LocaleSidKey      = 'en_US';
    mgr.EmailEncodingKey  = 'ISO-8859-1';
    mgr.LanguageLocaleKey = 'en_US';
    insert mgr;

    // Create user w/above manager
    User owner              = new User();
    owner.Username          = 'govlimitqueen@gmail.com';
    owner.Email             = 'govlimitqueen@gmail.com';
    owner.FirstName         = 'No';
    owner.LastName          = 'Prenup';
    owner.Alias             = 'dollaz';
    owner.CommunityNickname = 'cashmunny';
    owner.ProfileId         = '00ei0000000rTfp';
    owner.TimeZoneSidKey    = 'GMT';
    owner.LocaleSidKey      = 'en_US';
    owner.EmailEncodingKey  = 'ISO-8859-1';
    owner.LanguageLocaleKey = 'en_US';
    owner.ManagerId         = mgr.Id;
    insert owner;
    
    // Create 200 opps to test gov limits
    System.runAs(owner) {
      List<Opportunity> opps = new List<Opportunity>();
      for (Integer i = 0; i < 200; i++) {
        Opportunity o = new Opportunity();
        o.Name        = 'Biggest Deal of All Time';  
        o.Amount      = 10;
        o.CloseDate   = Date.today();
        o.StageName   = 'Prospecting';
        opps.add(o);
      }
      insert opps;
    }
    
    // Make sure everything worked!
    List<Opportunity> newOpps = [SELECT Id, Owner_Manager__c
                                  FROM Opportunity];
    for (Opportunity o : newOpps) {
      System.assertEquals(mgr.Id, o.Owner_Manager__c);
    }
  }
}