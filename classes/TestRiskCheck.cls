@isTest
public class TestRiskCheck {
    static testMethod void testDupes() {
    
    //Let's create our record from scratch!
    Risk_Assessment__c risk = new Risk_Assessment__c();
    risk.Name = 'Dave';
    risk.Account__c = '001o000000Po3Ki';
    risk.Current__c = True;
    
    
    //This is a special wayu of doing "risky" things
    try {
        insert risk;
    } catch (Exception e) {
        System.debug('An error happened, as predicted!');        
    }
    
    //now we try to find our duple lead by email
    List<Risk_Assessment__c> dupes = [Select Id From Risk_Assessment__c
                        Where Current__c = True];
    System.assertEquals(1,dupes.size());                    
    
     //Let's create our record from scratch!
    Risk_Assessment__c risk1 = new Risk_Assessment__c();
    risk1.Name = 'Dave1';
    risk1.Account__c = '001o000000Po3Ki';
    risk1.Current__c = True;
     insert risk1;
    
    Risk_Assessment__c risk2 = new Risk_Assessment__c();
    risk2.Name = 'Dave2';
    risk2.Account__c = '001o000000Po3Ki';
    risk2.Current__c = True;
     insert risk2;
    
    List<Risk_Assessment__c> dupes1 = [Select Id From Risk_Assessment__c
                        Where Current__c = False];
    System.assertEquals(2,dupes1.size());  
    }
}