public class RandomContactFactory {
    public static List<Contact> generateRandomContacts(Integer numContacts, string ConLN) {
        List<Contact> cons = new List<Contact>();
        
        for(Integer i=0;i<numContacts;i++) {
            Contact c = new Contact(FirstName= ConLN + ' ' + i );
            cons.add(c);
        }
       // insert cons;
       return cons; 
    }     
}