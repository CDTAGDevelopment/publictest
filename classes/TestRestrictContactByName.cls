@isTest
    private class TestRestrictContactByName {
    
        
 
         @isTest static void InvalidLastName() {
           Contact con = new Contact(LastName ='INVALIDNAME');
           insert con;
                
           Test.startTest();
          	 Database.SaveResult results = Database.insert(con, false);  
           Test.stopTest();  
           System.assert(!results.isSuccess());  
           System.assertEquals('The Last Name "'+con.LastName+'" is not allowed for DML',results.getErrors()[0].getMessage()); 
       }
       
         @isTest static void testLastName() {
           Contact ct = new Contact(LastName='Joners');
           insert ct;
                
           Test.startTest();
        	   Database.SaveResult results = Database.insert(ct, false);  
           Test.stopTest();  
           System.assert(results.isSuccess());         
       }
        
        
 }