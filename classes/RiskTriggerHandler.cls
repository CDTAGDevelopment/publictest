public with sharing class RiskTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
   // public Set<Id> setAccIds;   
   // public Map<Id, Risk_Assessment__c> mapRAObjs;
     // Grab your Custom Setting values 

  
   
    //Account_Case_Method__c ACM = Account_Case_Method__c.getInstance('OnOff');
    //String Switched1 = ACM.Switched_1__c;
    //String Switched2 = ACM.Swtiched_2__c;
    //System.debug(System.LoggingLevel.ERROR, Switched1);
    //system.debug(Switched2);    


    
    public RiskTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
        
    public void OnBeforeInsert(Risk_Assessment__c[] newRisks){
    }
    
    
    public void OnAfterInsert(Risk_Assessment__c[] newRisks){
         
    Map<Id, Risk_Assessment__c> mapRAObjs = new Map<Id, Risk_Assessment__c>();
    List<Risk_Assessment__c> riskList = new List<Risk_Assessment__c>();
    Set<Id> setAccIds=new Set<Id>();
    
        For(Risk_Assessment__c riskObj : newRisks){
            if(riskObj.Current__c){
              setAccIds.add(riskObj.Account__c);
              mapRAObjs.put(riskObj.Id, riskObj);
            }        
        }

    List<Risk_Assessment__c> Risks = [SELECT Id, Name, Account__c,Current__c FROM Risk_Assessment__c where Account__c In :setAccIds];

    // For loop to iterate through all the queried Account records
        for(Risk_Assessment__c risk: Risks){
            if(mapRAObjs.containsKey(risk.Id)==False){
                if(risk.Current__c == True)
                    risk.Current__c = False;
                    riskList.add(risk);
            }
        }
    update riskList;
    }
    
    

    public void OnBeforeUpdate(Risk_Assessment__c[] oldRisks, Risk_Assessment__c[] updatedRisks, Map<ID, Risk_Assessment__c> riskMap){
           
        Map<Id, Risk_Assessment__c> mapRAObjs = new Map<Id, Risk_Assessment__c>();
        List<Risk_Assessment__c> riskList = new List<Risk_Assessment__c>();
        Set<Id> setAccIds=new Set<Id>();
        
            For(Risk_Assessment__c riskObj : updatedRisks){
                if(riskObj.Current__c){
                  setAccIds.add(riskObj.Account__c);
                  mapRAObjs.put(riskObj.Id, riskObj);
                }        
            }

        List<Risk_Assessment__c> Risks = [SELECT Id, Name, Account__c,Current__c FROM Risk_Assessment__c where Account__c In :setAccIds];
    
        // For loop to iterate through all the queried Account records
            for(Risk_Assessment__c risk: Risks){
                if(mapRAObjs.containsKey(risk.Id)==False){
                    if(risk.Current__c == True)
                        risk.Current__c = False;
                        riskList.add(risk);
                }
            }
        update riskList;
    }
    

    
    public void OnAfterUpdate(Risk_Assessment__c[] oldRisks, Risk_Assessment__c[] updatedRisks, Map<ID, Risk_Assessment__c> riskMap){
    
    }
    
 //   @future public static void OnAfterUpdateAsync(Set<ID> updatedAccountIDs){
   //     List<Account> updatedAccounts = [select Id, Name from Account where Id IN :updatedAccountIDs];
   // }
    
   // public void OnBeforeDelete(Account[] accountsToDelete, Map<ID, Account> accountMap){ 
    
     /*   Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
            for(Account delacct : accountsToDelete){
                if(profileName != 'Test') {
                delAcct.addError('Cannnot delete it');
                //delAcct.AnnualRevenue.addError('Cannnot delete');
                //newAccount.addError('Missing annual revenue');
                }
            }      
     */                
  }