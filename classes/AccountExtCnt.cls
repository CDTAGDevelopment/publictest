public class AccountExtCnt {
    //This is a test.
    private final Account acc;
    public List<Account> accList {get; set;}
    public List<Contact> conList {get; set;}
    public List<Opportunity> oppList {get; set;}
    
    public AccountExtCnt(ApexPages.StandardController std){
        this.acc = (Account)std.getRecord();
        Account tempAacc = [select Id, Name, Type, RecordType.Name, RecordTypeId from Account where Id = :acc.Id];
        
        List<String> accTypeList = new List<String>();
        if('Client'==tempAacc.Type){
            accTypeList.add('Employer');
            accTypeList.add('Location');
        } else if('Employer'==tempAacc.Type){
            accTypeList.add('Location');
        }
            
        //this.accList = [select Id, Name, Type, RecordType.Name from Account where ParentId = :acc.Id and Type Like : accTypeList];
        this.accList = getRoleSubordinateUsers(acc.Id);
        system.debug('****************** accList: '+ accList);
        accList.add(acc);
        this.conList = [select Id, Name, Account.Name, AccountId, Account.Type from Contact where AccountId IN :accList];
        
        this.oppList = [select Id, Name, Account.Name, AccountId, Account.Type from Opportunity where AccountId IN :accList];
    }
    
    public static List<Account> getRoleSubordinateUsers(Id accId) {
        // get requested user's role
        List<Account> accList  = [select Id, Name, Type, RecordType.Name, RecordTypeId from Account where ParentId = :accId and ParentId!=null];
        Set<Id> accIdSet = new Set<Id>();
        for(Account acc : accList){
            accIdSet.add(acc.Id);
        }
        // get all of the roles underneath the user
        Set<Id> allSubRoleIds = getAllSubRoleIds(accIdSet);
        
        allSubRoleIds.addAll(accIdSet);
        
        
        // get all of the ids for the users in those roles
        Map<Id,Account> users = new Map<Id, Account>([select Id, Name, Type, RecordType.Name, RecordTypeId from Account where Id IN :allSubRoleIds]);
        // return the ids as a set so you can do what you want with them
        //return users.keySet();
        
        return users.values();
    }
    
    private static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
        
        // get all of the roles underneath the passed roles
        for(Account userRole :[select Id, Name, Type, RecordType.Name, RecordTypeId from Account where ParentId IN :roleIds AND ParentId!=null])
            currentRoleIds.add(userRole.Id);
            
        // go fetch some more rolls!
        if(currentRoleIds.size() > 0)
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        
        return currentRoleIds;
    }
    /*
    public static Set<ID> getRoleSubordinateUsers(Id userId) {
        // get requested user's role
        Id roleId = [select UserRoleId from User where Id = :userId].UserRoleId;
        // get all of the roles underneath the user
        Set<Id> allSubRoleIds = getAllSubRoleIds(new Set<ID>{roleId});
        // get all of the ids for the users in those roles
        Map<Id,User> users = new Map<Id, User>([Select Id, Name From User where UserRoleId IN :allSubRoleIds]);
        // return the ids as a set so you can do what you want with them
        return users.keySet();
    }
    
    private static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
        Set<ID> currentRoleIds = new Set<ID>();
        
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[select Id from UserRole where ParentRoleId IN :roleIds AND ParentRoleID != null])
            currentRoleIds.add(userRole.Id);
            
        // go fetch some more rolls!
        if(currentRoleIds.size() > 0)
            currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
        
        return currentRoleIds;
    }*/
}