@isTest
private class TestVerifyDate {
     @isTest static void testDates() {
        Date myDate1 = Date.newInstance(2015, 3, 10);
        Date myDate2 = Date.newInstance(2015, 3, 20);
        Date finalDate = VerifyDate.CheckDates(myDate1, myDate2);   
        System.assertEquals(myDate2, finalDate);
   
     }
     
      @isTest static void testDates30() {
        Date myDate11 = Date.newInstance(2015, 3, 10);
        Date myDate22 = Date.newInstance(2015, 5, 20);
        Date finalDates = VerifyDate.CheckDates(myDate11, myDate22);  
        date expectedDate = date.newInstance(2015, 3, 31);
        System.assertEquals(expectedDate, finalDates);
   
     }
}