@isTest
public class TestpreventClose {
       static testMethod void preventClose() {
       
       //create opp
       Opportunity o = new Opportunity();
       o.Name = 'Sample123';
       o.CloseDate =  Date.today()+2;
       o.StageName = 'Prospecting';
       o.TrackingNumber__c = '1';
       insert o;
       
        Task t = new Task();
        t.Subject = 'Run Test Trigger';
        t.Status = 'Not Started';
        t.Priority = 'Normal';
        t.WhatId = o.Id;
        insert t;
        
         o.StageName = 'Closed Won';
          
         try {
             update o;
         } catch (Exception e) {
            System.debug('An error happened, as predicted!');        
          }
        
         t.Status = 'Completed';
         update t;
         
         o.StageName = 'Closed Won';
         try {
             update o;
         } catch (Exception e) {
            System.debug('An error happened, as predicted!');        
          }
        
          System.assertEquals(o.StageName, 'Closed Won');
           
       }
   
   

   
   
      
}