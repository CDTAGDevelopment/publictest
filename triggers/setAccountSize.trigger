trigger setAccountSize on Contact (before insert, before delete, before update) {

//get a string of all account ids
Set<String> cts = new Set<String>();

 if (Trigger.isDelete) {
    for (Contact c : Trigger.old) {
        cts.add(c.AccountId);
    }
 } else if (Trigger.isInsert) {
    for (Contact c : Trigger.new) {
        cts.add(c.AccountId);
    }
 } else {
    for (Contact c : Trigger.new) {
        //Set variable to old account id
        Contact cOld = System.Trigger.oldMap.get(c.AccountId);
        if (cOld.AccountId != c.AccountId) {
            cts.add(c.AccountId);
            cts.add(cOld.AccountId);
        } else {
            cts.add(c.AccountId);
        }
    }
 }
          
//Now we need to build a lit to quesry and get the asscoiated account ids
//List<Account> accountSize = new List<Account>();
AggregateResult[] contactCountQuery = [SELECT count(id), AccountId FROM Contact WHERE AccountId in : cts GROUP BY AccountId];



//Create a map so we can do a lookup
//Getting the account id from the contact record so we can query the number of contacts
Map<Id, Integer> contactCountMap = new Map<Id, Integer>();

    for (AggregateResult ar : contactCountQuery) {
        contactCountMap.put((Id)ar.get('AccountId'), (Integer)ar.get('expr0'));
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    List < Account > accts = [select Id, Rating from Account Where Id in : contactCountMap.keySet()for update];

    if (contactCountMap.isEmpty())
        return;
    // Now go through them all and set the MPS_Record__c to the Id of the new MPS record returned from the map.
    for (Account acct : accts) {
        if (contactCountMap.get(acct.Id) >= 2) {
            acct.Rating = 'Hot';
        } else {
            acct.Rating = 'Cold';
        }
    }
}