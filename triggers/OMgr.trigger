//Write a trigger that populates an “Owner’s Manager” lookup field on opportunities based 
//on the opp owner’s (user’s) standard ManagerId field.

trigger OMgr on Opportunity (before insert, before update) {
  // Get the set of all owners
  Set<Id> ownerIds = new Set<Id>();
  for (Opportunity opp : Trigger.new) {
    ownerIds.add(opp.OwnerId);
  }
  
  // Create a map of users to managers  
  List<User> users = [SELECT Id, ManagerId 
                       FROM User 
                       WHERE Id IN :ownerIds];
  Map<Id, Id> userToMgr = new Map<Id, Id>();
  for (User u : users) {
    userToMgr.put(u.Id, u.ManagerId);
  }
  
  // Set the opp field to the owner's manager
  for (Opportunity o : Trigger.new) {
    o.Owner_Manager__c = userToMgr.get(o.OwnerId);
  }
}