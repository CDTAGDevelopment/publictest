trigger Farewell on Lead (before insert, before update) {
  // Grab your Custom Setting values
 //Bye_Settings__c settings = Bye_Settings__c.getInstance('bye');
 // Boolean triggerIsOff     = settings.Turn_Off_Trigger__c;
  
  // Make sure trigger is on
  //if (!triggerIsOff) {
    System.debug('Farewell trigger is on!');
    
    // Create master email list
    List<Messaging.SingleEmailMessage> mails = 
    new List<Messaging.SingleEmailMessage>();
    
    // Loop through all leads in trigger
    for (Lead myLead : Trigger.new) {
      if (myLead.Email != null) {
        if (myLead.Rating == 'Cold') {

          Boolean sendEmail = false;

          // Inserted leads have no "old" value
          if (Trigger.isInsert) {
            sendEmail = true;
            System.debug(myLead.Email + ' is new and cold.');
          // Check that updated leads were changed to 'Cold'
          } else {
            if (Trigger.oldMap.get(myLead.Id).Rating != 'Cold') {
              sendEmail = true;
              System.debug(myLead.Email + ' is now cold.');
            }
          }
    
          // Create the email
          if (sendEmail) {
            System.debug('Creating email for ' + myLead.Email);
            Messaging.SingleEmailMessage mail = 
            new Messaging.SingleEmailMessage();
            List<String> sendTo = new List<String>();
            sendTo.add(myLead.Email);
            mail.setToAddresses(sendTo);
            mail.setReplyTo(UserInfo.getUserEmail());
            mail.setSenderDisplayName(UserInfo.getName());
            mail.setSubject('Farewell, my love.');
            String body = 'Life is too short.';
            mail.setHtmlBody(body);
            mails.add(mail);
          }
        }
      }
    }
    Messaging.sendEmail(mails);
    System.debug(mails.size() + ' emails sent!');
 // } else {
 //   System.debug('Farewell trigger is off.');
  }
//}