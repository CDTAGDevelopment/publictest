trigger GetApprovers on Quote (after insert) {

//query list above to get owner ids from opps ids
List<Quote> ques = [Select q.Id, q.Opportunity.Owner.Pricing_Team__c, q.Opportunity.Owner.RM_Director__c , q.Opportunity.Owner.Pricing_Director__c From Quote q where q.id IN :Trigger.newMap.keySet()];

for (Quote quot : ques) {
    quot.Pricing_Team__c = quot.Opportunity.Owner.Pricing_Team__c;
    quot.RM_Director__c = quot.Opportunity.Owner.RM_Director__c;
    quot.Pricing_Director__c = quot.Opportunity.Owner.Pricing_Director__c;
    }
  update ques;
}