trigger UpdateLeadStatus on task(after insert, after update) {

    //Get the lead prefix one time
    String leadPrefix = Schema.SObjectType.Lead.getKeyPrefix();

    // Step 1: Create a set of all Lead Ids of Tasks to query
    set < id > leadId = new set < id > ();
    
    for (Task ts : trigger.new) {
        //check to see if the whoid has a value first
        if (ts.whoId != null) {
            //add the lead id to the set if the task is attached to a Lead object
            if (String.valueOf(ts.whoId).startsWith(leadPrefix) == TRUE) {
                leadId.add(ts.whoId);            
            }
        }
    }
    
    //List of full leads/fields when status is open not contacted
    List<Lead> workingLeads = [Select Id, Status from Lead where id in :leadId and Status = 'Open - Not Contacted'];
   
    //Grab the # of tasks associated to the leads and only return the lead that only have 1 task 
    AggregateResult[] taskCount = [SELECT WhoId, count(id) FROM Task where whoId in :leadId AND WhoId != null GROUP BY WhoId Having count(id)=1];
    
    //define map to hold lead id and associated task count
    Map<Id, Integer> taskCountMap = new Map<Id, Integer>();
    
    //Loop thru the AggregateResult[] query and place in lead id and task count in the mnap
    for (AggregateResult ar : taskCount )  {
        taskCountMap.put((Id)ar.get('whoId'), (Integer)ar.get('expr0'));
    }
    
    //Loop thru all the leads that Open Not Contacted status 
    for (Lead taskLead : workingLeads) {
        //do the vlookup
      if (taskCountMap.get(taskLead.id) == 1){
            taskLead.Status = 'Working - Contacted';            
            update taskLead;
      }
    }
}