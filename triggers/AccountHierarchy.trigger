trigger AccountHierarchy on Task (before insert, before update) {

    
    //Query based on the whatid to get the account hierarchy field
    //place that in the account hierarchy field
    
    Set<Id> oppsId = new Set<Id>();
    Set<Id> accId = new Set<Id>();
    
    //Create a list of just task that needs to be updated to use later
    List<Task> taskList = new List<Task>();
    
    //Need to get the what id for opps only that are related to the task
    for(Task t : trigger.new){
        String wId = t.WhatId;
            //Opportunity
            if(wId != NULL && wId.startsWith('006') && !oppsId.contains(t.WhatId)) {
                oppsId.add(t.WhatId);
                //Add to talk list
                taskList.add(t);
            }
            //Account
            if(wId != NULL && wId.startsWith('001') && !accId.contains(t.WhatId)) {
                accId.add(t.WhatId);
                //Add to talk list
                taskList.add(t);
            }
    }
    
    //Query to return a list of associated opps 
    List<Opportunity> taskOps = [Select Id, Account_Hierarchy__c from Opportunity 
                                Where Id in :oppsId];
                                
    //Query to return a list of associated accounts                             
    List<Account> taskAcc = [Select Id, RecordType.Name, Account_Hierarchy__c from Account 
                                Where Id in :accId];   
                                                        
    //Add them to a Opportunity map                            
    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>(taskOps); 
    //Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>();     
        //for(Opportunity o : taskOps){
        //    oppMap.put(o.id,o);
        //}                           
  
    //Add them to a Account map                            
    Map<Id, Account> accMap = new Map<Id, Account>(taskAcc);
    //Map<Id, Account> accMap = new Map<Id, Account>();        
       // for(Account a : taskAcc){
       //     accMap.put(a.id,a);
       // } 
  
       //Can we make this run offof something other than trigger.new since not all tasks will need to be updated 
       //for(Task t : trigger.new){
       for(Task t : taskList){
        String wId = t.WhatId;
            if(wId != NULL && wId.startsWith('006')) {
                Opportunity thisOpp = oppMap.get(t.WhatId);
                    if(thisOpp!=null) {
                       t.Account_Hierarchy__c = thisOpp.Account_Hierarchy__c;
                       t.Related_To__c = 'Opportunity';
                    }
            }
            if(wId != NULL && wId.startsWith('001')) {
                Account thisAcc = accMap.get(t.WhatId);
                    if(thisAcc!=null) {
                       t.Account_Hierarchy__c = thisAcc.Account_Hierarchy__c;
                       t.Related_To__c = thisAcc.RecordType.Name;
                    }
            } 
       }     
         
}