trigger UpdateCurrent on Risk_Assessment__c (before insert,before update) {

Map<Id, Risk_Assessment__c> mapRAObjs = new Map<Id, Risk_Assessment__c>();
  
List<Risk_Assessment__c> riskList = new List<Risk_Assessment__c>();

Set<Id> setAccIds=new Set<Id>();

        For(Risk_Assessment__c riskObj : trigger.new){
            if(riskObj.Current__c){
              setAccIds.add(riskObj.Account__c);
              mapRAObjs.put(riskObj.Id, riskObj);
            }        
        }


List<Risk_Assessment__c> Risks = [SELECT Id, Name, Account__c,Current__c FROM Risk_Assessment__c where Account__c In :setAccIds];

// For loop to iterate through all the queried Account records
for(Risk_Assessment__c risk: Risks){
if(mapRAObjs.containsKey(risk.Id)==False){
 if(risk.Current__c == True)
        risk.Current__c = False;
        riskList.add(risk);
}
}
update riskList;

}