trigger preventClose on Opportunity (before update) {
//Get a string of all Opportunity ids that have been just updated to Closed Won

String sBreak = '<br//>';

Set<String> opps = new Set<String>();
    for (Opportunity opp : Trigger.new) {
          Opportunity oldOpp = Trigger.oldMap.get(opp.Id);

        // Trigger.new records are conveniently the "new" versions!
        Boolean oldOppIsWon = oldOpp.StageName.equals('Closed Won');
        Boolean newOppIsWon = opp.StageName.equals('Closed Won');
    
        // Check that the field was changed to the correct value
        if (!oldOppIsWon && newOppIsWon) {
            opps.add(opp.Id);
        }
    }
       
List<Task> dupes = [SELECT Id, Subject, Status FROM Task WHERE WhatId in :opps];
    
AggregateResult[] taskCountQuery = [SELECT WhatId, count(id)sampleLabel FROM Task WHERE WhatId in :opps AND Status != 'Completed' GROUP BY WhatId];

Map<Id, Integer> taskCountMap = new Map<Id, Integer>();

    for (AggregateResult ar : taskCountQuery) {
           taskCountMap.put((Id)ar.get('WhatId'), (Integer)ar.get('sampleLabel'));
    }

 if (taskCountMap.isEmpty())
        return;

     for (Opportunity oppt : trigger.new) {
        if (taskCountMap.get(oppt.Id) >= 1) {
            String errorMessage = 'You may not close this opportunity until all task(s) are completed.' + sBreak; 
            for (Task c : dupes){
             if (c.Status != 'Completed') {
              errorMessage += '<li//>Subject is: ' + c.Subject + sBreak;
             }
            }
            oppt.StageName.addError(errorMessage,false);              
        }
     }
}